from tilepuzzle.node import AStarNode
from tilepuzzle.board import Board
import pdb

b = Board([7, 2, 4, 5, 0, 6, 8, 3, 1])
goal = Board([0, 1, 2, 3, 4, 5, 6, 7, 8])

pdb.set_trace()

a = AStarNode(b, goal)
pdb.set_trace()
