from setuptools import setup

setup(
    name='sliding-puzzle',
    version='1.0.0',
    author_email='christianbboman@gamil.com',
    install_requires=[
        'numpy'
    ]
)