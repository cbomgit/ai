How to solve?
1) Breadth first 2) Graph Search (uniform cost) 3) A*
Enter the number corresponding to the search type
Enter path to game data file: Nodes explored: 
467
[0, 1, 2, 3, 4, 5, 6, 7, 8]
[3, 1, 2, 0, 4, 5, 6, 7, 8]
[3, 1, 2, 4, 0, 5, 6, 7, 8]
[3, 1, 2, 4, 7, 5, 6, 0, 8]
[3, 1, 2, 4, 7, 5, 6, 8, 0]
[3, 1, 2, 4, 7, 0, 6, 8, 5]
[3, 1, 0, 4, 7, 2, 6, 8, 5]
[3, 0, 1, 4, 7, 2, 6, 8, 5]
[3, 7, 1, 4, 0, 2, 6, 8, 5]
[3, 7, 1, 0, 4, 2, 6, 8, 5]
Solution length: 11
