from collections import deque,namedtuple
from tilepuzzle.board import Board
import queue
import pdb

class SearchNode(namedtuple("SearchNode", ["board", "parent", "action"])):

    """
    represents a search node in the search algorithm. This object has a
    board object to represent its board state, a parent attribute to
    represent the parent, and a rules list to keep track of the rules
    left to process
    """

    __slots__ = ()

    def __new__(cls, board, parent=None, action=None):
        return super().__new__(cls, board, parent, action)

    def __hash__(self):
        return self.board.__hash__()

    def __eq__(self, other):
        return isinstance(other, SearchNode) and self.board == other.board

    def __ne__(self, other):
        return self.board != other.board

class GraphNode(namedtuple("GraphNode", ["board", "parent", "action", "cost"])):

    """
    Represents a node in a graph search algorithm. This object has a board object 
    to represent its board state, a parent attribute to represent the parent, and
    a rules list to keep track of the rules left to process
    """

    __slots__ = ()

    def __new__(cls, board, parent=None, action=None, cost=None):
        return super().__new__(cls, board, parent, action, cost)
    
    def __hash__(self):
        return self.board.__hash__()

    def __eq__(self, other):
        return isinstance(other, GraphNode) and self.board == other.board
    
    def __ne__(self, other):
        return self.board != other.board

    def __lt__(self, other):
        return self.cost < other.cost
 
class AStarNode(namedtuple("AStarNode", ["board", "parent", "action", "cost"])):

    """
    Represents a node in a graph search algorithm. This object has a board object 
    to represent its board state, a parent attribute to represent the parent, and
    a rules list to keep track of the rules left to process
    """

    __slots__ = ()

    def __new__(cls, board, goal, parent=None, action=None):

        cost = manhattan_distance(board.board, goal) + misplaced_tile_cost(board.board, goal.board)
        return super().__new__(cls, board, parent, action, cost)
    
    def __hash__(self):
        return self.board.__hash__()

    def __eq__(self, other):
        return isinstance(other, AStarNode) and self.board == other.board
    
    def __ne__(self, other):
        return self.board != other.board

    def __lt__(self, other):
        return self.cost < other.cost

    
def misplaced_tile_cost(board, goal):

    num_displaced = 0

    for i,j in zip(board, goal):
        if i is not 0:
            if i != j:
                num_displaced += 1

    return num_displaced
    
def manhattan_distance(board, goal):

    manhattan_distance = 0

    for i in board:
        if i is not 0:
            tile_ndx = board.index(i)

            if i not in goal.board:
                pdb.set_trace()
            
            goal_ndx = goal.board.index(i)
            row_distance = abs(int(tile_ndx / 3) - int(goal_ndx / 3))
            col_distance  = abs(tile_ndx % 3 - goal_ndx % 3)
            manhattan_distance += row_distance + col_distance

    return manhattan_distance
