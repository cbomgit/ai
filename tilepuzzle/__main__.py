from tilepuzzle import solver 

print("How to solve?")
print("1) Breadth first 2) Graph Search (uniform cost) 3) A*")
print("Enter the number corresponding to the search type")

search_type = int(input().strip())
result = None

if search_type is 1:
    result = solver.dfs()
elif search_type is 2:
    result = solver.graph_search()
elif search_type is 3:
    result = solver.a_star_search()
else:
    print("not a valid search type. exiting")


if result is not None:
    node = result
    num_nodes = 1
    while node.parent is not None:

        print(node.board.board)
        node = node.parent
        num_nodes += 1

print("Solution length: " + str(num_nodes))