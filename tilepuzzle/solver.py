from collections import deque,namedtuple
from tilepuzzle.board import Board
from tilepuzzle.node import SearchNode,GraphNode,AStarNode
import queue
import json

def read_game_data(file_name='tilepuzzle/data/2-moves.json'):

    """
    takes a file name as a parameter and loads the game data into a dict
    """
    with open(file_name) as data_file:
        game_data = json.load(data_file)

    return game_data

def dfs():

    game_data = read_game_data(input("Enter path to game data file: ").strip())
    goal = Board([item for row in game_data['goal'] for item in row])
    initial = Board([item for row in game_data['start'] for item in row])

    node = SearchNode(initial)
    visited = set()
    stack = []
    stack.append(node)

    while stack:

        node = stack.pop()
        visited.add(node)

        moves = node.board.get_valid_moves()
        for move in moves:
            successor = SearchNode(node.board.get_successor(move), node, move)
            if successor.board == goal:
                print("Nodes explored: ")
                print(len(visited))
                return successor
            if successor not in visited:
                stack.append(successor)

    return None

def graph_search():

    game_data = read_game_data(input("Enter path to game data file: ").strip())
    goal = Board([item for row in game_data['goal'] for item in row])
    initial = Board([item for row in game_data['start'] for item in row])

    node = GraphNode(initial, None, None, 0)
    frontier = queue.PriorityQueue()
    explored = set()

    frontier.put(node)
    while not frontier.empty():

        node = frontier.get()

        if node.board == goal:
            print("Nodes explored: ")
            print(len(explored))
            return node

        explored.add(node.board)
        
        moves = node.board.get_valid_moves()

        for move in moves:

            child = GraphNode(node.board.get_successor(move), node, move, node.cost + 1)

            if child.board not in explored and child not in frontier.queue:
                frontier.put(child)
            elif child in frontier.queue:
                n = frontier.queue.index(child)
                if frontier.queue[n].cost < child.cost:
                    frontier.queue.remove(frontier.queue[n])
                    frontier.put(child)

    return None

def a_star_search():

    game_data = read_game_data(input("Enter path to game data file: ").strip())
    goal = Board([item for row in game_data['goal'] for item in row])
    initial = Board([item for row in game_data['start'] for item in row])

    node = AStarNode(initial,goal, None, None)
    frontier = queue.PriorityQueue()
    explored = set()

    frontier.put(node)
    while not frontier.empty():

        node = frontier.get()

        if node.board == goal:
            print("Nodes explored: ")
            print(len(explored))
            return node

        explored.add(node.board)
        
        moves = node.board.get_valid_moves()

        for move in moves:

            child = AStarNode(node.board.get_successor(move), goal, node, move)

            if child.board not in explored and child not in frontier.queue:
                frontier.put(child)
            elif child in frontier.queue:
                n = frontier.queue.index(child)
                if frontier.queue[n].cost < child.cost:
                    frontier.queue.remove(frontier.queue[n])
                    frontier.put(child)


    return None
