from math import sqrt
from collections import namedtuple
import pdb

class Board(namedtuple("Board", ["board", "size", "zero_at"])):
    """
    A class representing a sliding tile board
    """

    UP = 'up'
    DOWN = 'down'
    LEFT = 'left'
    RIGHT = 'right'

    __slots__ = ()

    def __new__(cls, board, width=None, zero_at=None):


        zero_at = zero_at or board.index(0)
        width = width or int(sqrt(len(board)))

        return super().__new__(cls, board, width, zero_at)

    def __hash__(self):
        return hash(str(self.board))

    def get_valid_moves(self):
        """
        Return a list of moves that can be applied to the current
        board
        """
        moves = []

        row = int(self.zero_at / self.size)
        col = self.zero_at % self.size

        if row > 0:
            moves.append(Board.UP)

        if row < self.size - 1:
            moves.append(Board.DOWN)

        if col > 0:
            moves.append(Board.LEFT)

        if col < self.size - 1:
            moves.append(Board.RIGHT)

        return moves

    def get_successor(self, move):

        """
        Applies move (up, right, down, left) to the board
        and returns a new board with the new configuraiton
        """
        successor = self.board.copy()

        if move is Board.UP:
            target = abs(self.zero_at - self.size)
        elif move is Board.DOWN:
            target = self.zero_at + self.size
        elif move is Board.LEFT:
            target = abs(self.zero_at - 1)
        else:
            target = self.zero_at + 1

        tmp = successor[target]
        successor[target] = successor[self.zero_at]
        successor[self.zero_at] = tmp

        return Board(successor)


    def pprint(self):

        """
        pretty prints the board
        """
        for i in range(0, len(self.board), self.size):
            print(self.board[i:i+self.size])

